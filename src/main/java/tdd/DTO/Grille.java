package tdd.DTO;

import lombok.Data;

@Data
public class Grille {

    String[] grille = { "-------", "-------", "-------", "-------", "-------", "-------" };

    public Grille() {

    }

    public Grille(String[] grille) {
        super();
        this.grille = grille;
    }

    public String display() {

        String s = "";
        for (int i = 0; i < grille.length; i++) {
            s = s + grille[i] + " ";
        }
        return s;
    }

    public void addToken(Long indexC, char token) {

        for (int i = grille.length - 1; i >= 0; i--) {
            String s = grille[i];
            char c = s.charAt(indexC.intValue() - 1);
            if (c == '-') {
                StringBuffer str = new StringBuffer();
                str.append(grille[i]);
                str.setCharAt(indexC.intValue() - 1, token);
                grille[i] = str.toString();

                break;
            }
        }

    }

    public char getCellue(Long indexL, Long indexC) {
        return grille[indexL.intValue() - 1].charAt(indexC.intValue() - 1);
    }

}
