package tdd.DTO;

public class Analyse {

    public Analyse() {
    }

    public Boolean analyseGrille(Grille grille) {

        Boolean flagColumn = analyseColumn(grille);

        Boolean flagLigne = analyseLigne(grille);

        Boolean flagDiagonalR = analyseDiagonalR(grille);

        Boolean flagDiagonalL = analyseDiagonalL(grille);

        return flagColumn || flagLigne || flagDiagonalR || flagDiagonalL;
    }

    private Boolean analyseDiagonalL(Grille grille) {
        String s = "";
        for (int j = 7; j >= 4; j--) {
            int m = j;
            for (int i = 6; i > 0; i--) {
                if (m >= 1) {
                    s = s + grille.getCellue(Long.valueOf(i), Long.valueOf(m));
                    m = m - 1;
                }
            }
        }

        return s.contains("OOOO") || s.contains("****");
    }

    private Boolean analyseDiagonalR(Grille grille) {
        String s = "";
        for (int j = 1; j <= 4; j++) {
            int m = j;
            for (int i = 6; i > 0; i--) {
                if (m <= 7) {
                    s = s + grille.getCellue(Long.valueOf(i), Long.valueOf(m));
                    m = m + 1;
                }
            }
        }
        return s.contains("OOOO") || s.contains("****");
    }

    private Boolean analyseLigne(Grille grille) {
        String s = "";
        for (int i = 6; i > 0; i--) {
            s = s + grille.grille[i - 1] + " ";
        }
        return s.contains("OOOO") || s.contains("****");
    }

    private Boolean analyseColumn(Grille grille) {
        String s = "";
        for (int j = 1; j <= 7; j++) {
            for (int i = 6; i > 0; i--) {
                s = s + grille.getCellue(Long.valueOf(i), Long.valueOf(j));
            }
        }
        return s.contains("OOOO") || s.contains("****");
    }
}
