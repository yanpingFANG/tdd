package tdd.DTO;

import lombok.Data;

@Data
public class FizzBuzz {
    private String numValue;

    private static String FIZZ = "fizz";
    private static String BUZZ = "buzz";
    private static String FIZZ_BUZZ = "fizzBuzz";

    public FizzBuzz() {
    }

    public String check(Integer num) {
        if (num % 3 == 0 && num % 5 == 0) {
            return FIZZ_BUZZ;
        }
        if (num % 3 == 0)
            return FIZZ;
        else if (num % 5 == 0)
            return BUZZ;
        return String.valueOf(num);
    }
}
