package tdd;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import tdd.DTO.FizzBuzz;

public class FizzBuzzTest {

    public FizzBuzz fizzBuzz = new FizzBuzz();

    /**
     * list de test
     * 
     * 1. test pour le cas généralement (1, 2, 4...)
     * 
     * 2. test pour les multiples de 3 (3, 6...)
     * 
     * 3. test pour les multiples de 5 (5, 10...)
     * 
     * 4. test pour les multiples de 15 (15, 30..)
     * 
     */

    @Test
    public void ReturnNumValueOne_withInputNumOne() {

        // given
        Integer givenNum = 1;
        String expected = "1";

        // when
        String actual = fizzBuzz.check(givenNum);

        // then
        assertEquals(expected, actual);
    }

    @Test
    public void ReturnNumValue_withInputNumRandom() {

        // given
        Integer givenNum = 2;
        String expected = "2";

        // when
        String actual = fizzBuzz.check(givenNum);

        // then
        assertEquals(expected, actual);
    }

    @Test
    public void ReturnFizz_withMultiplesDeTrois() {

        // given
        Integer givenNum = 3;
        String expected = "fizz";

        // when
        String actual = fizzBuzz.check(givenNum);

        // then
        assertEquals(expected, actual);
    }

    @Test
    public void ReturnBuzz_withMultiplesDeCinq() {

        // given
        Integer givenNum = 5;
        String expected = "buzz";

        // when
        String actual = fizzBuzz.check(givenNum);

        // then
        assertEquals(expected, actual);
    }

    @Test
    public void ReturnFizzBuzz_withMultiplesDeQuinze() {

        // given
        Integer givenNum = 15;
        String expected = "fizzBuzz";

        // when
        String actual = fizzBuzz.check(givenNum);

        // then
        assertEquals(expected, actual);
    }
}
