package tdd;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import tdd.DTO.Analyse;
import tdd.DTO.Grille;

public class AnalyseTest {

    private Analyse analyse = new Analyse();

    /**
     * Liste de test
     * 
     * identifier s'il y a un gagnant -> vrais ou faux
     * 
     * 1. grille est vide, il n'y a pas un gagnant -> faux
     * 
     * 2. trouver quatre tokens 'O' dans le 1 colone -> vrais
     * 
     * 3. trouver un token != 'O' parmis quatre tokens dans le 1 colone -> faux
     * 
     * 4. trouver quatre tokens 'O' dans le 2 colone -> vrais
     * 
     * 5. trouver quatre tokens '*' dans le 1 colone -> vrais
     * 
     * 6. trouver quatre tokens 'O' dans le 6 lines -> vrais
     * 
     * 3. trouver un token != 'O' parmis quatre tokens dans le 6 lines -> faux * 5.
     * trouver quatre tokens '*' dans le 6 lines -> vrais
     */

    @Test
    public void ReturnStateFalse_withEmptyGrid() {
        // given
        String[] grilleT = { "-------", "-------", "-------", "-------", "-------", "-------" };
        Grille grilleGiven = new Grille(grilleT);
        // when
        Boolean actual = analyse.analyseGrille(grilleGiven);
        // then
        assertFalse(actual);
    }

    @Test
    public void ReturnStateTrue_withFourTokensO() {
        // given
        String[] grilleT = { "-------", "-------", "O------", "O------", "O------", "O------" };
        Grille grilleGiven = new Grille(grilleT);

        // when
        Boolean actual = analyse.analyseGrille(grilleGiven);
        // then
        assertTrue(actual);

    }

    @Test
    public void ReturnStateFalse_withThreeTokensO() {
        // given
        String[] grilleT = { "-------", "-------", "-------", "O------", "O------", "O------" };
        Grille grilleGiven = new Grille(grilleT);

        // when
        Boolean actual = analyse.analyseGrille(grilleGiven);
        // then
        assertFalse(actual);

    }

    @Test
    public void ReturnStateTrue_withFourTokensStar() {
        // given
        String[] grilleT = { "-------", "-------", "*------", "*------", "*------", "*------" };
        Grille grilleGiven = new Grille(grilleT);

        // when
        Boolean actual = analyse.analyseGrille(grilleGiven);
        // then
        assertTrue(actual);

    }

    @Test
    public void ReturnStateFalse_withTokensStarAndO() {
        // given
        String[] grilleT = { "-------", "-------", "*------", "O------", "*------", "*------" };
        Grille grilleGiven = new Grille(grilleT);

        // when
        Boolean actual = analyse.analyseGrille(grilleGiven);
        // then
        assertFalse(actual);

    }

    @Test
    public void ReturnStateTrue_withTokensStarAndO() {
        // given
        String[] grilleT = { "*------", "*------", "*------", "*------", "O------", "*------" };
        Grille grilleGiven = new Grille(grilleT);

        // when
        Boolean actual = analyse.analyseGrille(grilleGiven);
        // then
        assertTrue(actual);

    }

    @Test
    public void ReturnStateTrue_withFourTokensStarInColumn2() {
        // given
        String[] grilleT = { "-------", "-------", "-*-----", "-*-----", "-*-----", "-*-----" };
        Grille grilleGiven = new Grille(grilleT);

        // when
        Boolean actual = analyse.analyseGrille(grilleGiven);
        // then
        assertTrue(actual);

    }

//    @Test
//    public void ReturnStateFalse_withFourTokensStarInTwoColumns() {
//        // given
//        String[] grilleT = { "-------", "-------", "-------", "-------", "**-----", "**-----" };
//        Grille grilleGiven = new Grille(grilleT);
//
//        // when
//        Boolean actual = analyse.analyseGrille(grilleGiven);
//        // then
//        assertFalse(actual);
//
//    }

    @Test
    public void ReturnStateTrue_withFourTokensOInOneLigne() {
        // given
        String[] grilleT = { "-------", "-------", "-------", "-------", "-------", "OOOO---" };
        Grille grilleGiven = new Grille(grilleT);

        // when
        Boolean actual = analyse.analyseGrille(grilleGiven);
        // then
        assertTrue(actual);

    }

    @Test
    public void ReturnStateFalse_withThreeTokensOAndOneStarInLigne() {
        // given
        String[] grilleT = { "-------", "-------", "-------", "-------", "-------", "OOO*---" };
        Grille grilleGiven = new Grille(grilleT);

        // when
        Boolean actual = analyse.analyseGrille(grilleGiven);
        // then
        assertFalse(actual);

    }

    @Test
    public void ReturnStateTrue_withFourTokensStarInLigne() {
        // given
        String[] grilleT = { "-------", "-------", "-------", "-------", "-------", "****---" };
        Grille grilleGiven = new Grille(grilleT);

        // when
        Boolean actual = analyse.analyseGrille(grilleGiven);
        // then
        assertTrue(actual);

    }

    @Test
    public void ReturnStateTrue_withFourTokensStarInOneLigne() {
        // given
        String[] grilleT = { "-------", "-------", "-------", "-------", "-------", "--****-" };
        Grille grilleGiven = new Grille(grilleT);

        // when
        Boolean actual = analyse.analyseGrille(grilleGiven);
        // then
        assertTrue(actual);

    }

    @Test
    public void ReturnStateFalse_withFourTokensOAndStarInLignes() {
        // given
        String[] grilleT = { "-------", "-------", "-------", "-------", "OOOO---", "**O*---" };
        Grille grilleGiven = new Grille(grilleT);

        // when
        Boolean actual = analyse.analyseGrille(grilleGiven);
        // then
        assertTrue(actual);

    }

    @Test
    public void ReturnStateFalse_withFourTokenStar() {
        // given
        String[] grilleT = { "-------", "-------", "-------", "-------", "**----*", "**---**" };
        Grille grilleGiven = new Grille(grilleT);

        // when
        Boolean actual = analyse.analyseGrille(grilleGiven);
        // then
        assertFalse(actual);

    }

    @Test
    public void ReturnStateTrue_withFourTokenDiagonal() {
        // given
        String[] grilleT = { "-------", "-------", "---O---", "--O*---", "-O**---", "O*OO---" };
        Grille grilleGiven = new Grille(grilleT);

        // when
        Boolean actual = analyse.analyseGrille(grilleGiven);
        // then
        assertTrue(actual);

    }

    @Test
    public void ReturnStateTrue_withFourTokenDiagonal2() {
        // given
        String[] grilleT = { "-------", "-------", "----O--", "---O*--", "--O**--", "-O*OO--" };
        Grille grilleGiven = new Grille(grilleT);

        // when
        Boolean actual = analyse.analyseGrille(grilleGiven);
        // then
        assertTrue(actual);

    }

    @Test
    public void ReturnStateTrue_withFourTokenDiagonal3() {
        // given
        String[] grilleT = { "------O", "-----O*", "----O**", "---O*OO", "---OOO*", "---OOO*", };
        Grille grilleGiven = new Grille(grilleT);

        // when
        Boolean actual = analyse.analyseGrille(grilleGiven);
        // then
        assertTrue(actual);

    }

    @Test
    public void ReturnStateTrue_withFourTokenDiagonalLeft() {
        // given
        String[] grilleT = { "-------", "-------", "O------", "*O-----", "**O----", "O**O---" };
        Grille grilleGiven = new Grille(grilleT);

        // when
        Boolean actual = analyse.analyseGrille(grilleGiven);
        // then
        assertTrue(actual);

    }

}
