package tdd;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import tdd.DTO.Grille;

public class GrilleTest {

    private Grille grille = new Grille();

    /**
     * liste de test:
     * 
     * 1. sur la grille, identifiez la ou les responsabilités
     * 
     * 2. d'autre liste de test
     * 
     * tout est vide
     * 
     * ajoute un jeton dans la colone 1, don il doit avoir un jeton dans la colone 1
     * 
     * ajouter un jeton dans d'autre colone avec d'autre couleur, donc il faut avoir
     * un jeton correpondante dans la grille
     * 
     * ajouter un jeton dans une cellule particulière
     */

    @Test
    public void ReturnVide_withGridNull() {

        // given
        String expected = "------- " + "------- " + "------- " + "------- " + "------- " + "------- ";

        // when
        String actual = grille.display();

        // then
        assertEquals(expected, actual);
    }

    @Test
    public void ReturnGrid_withATokenInColum1() {

        // given
        String expected = "------- ------- ------- ------- ------- O------ ";
        Long indexL = 5L;
        Long indexC = 1L;
        char token = 'O';
        // when
        grille.addToken(indexC, token);
        String actual = grille.display();

        // then
        assertEquals(expected, actual);
    }

    @Test
    public void ReturnGrid_withATokenInColumn2() {

        // given
        String expected = "------- ------- ------- ------- ------- -O----- ";
        Long indexL = 6L;
        Long indexC = 2L;
        char token = 'O';

        // when
        grille.addToken(indexC, token);
        String actual = grille.display();

        // then
        assertEquals(expected, actual);
    }

    @Test
    public void ReturnGrid_withTwoSameToken() {

        // given
        Long indexL1 = 5L;
        Long indexC1 = 2L;

        Long indexL2 = 4L;
        Long indexC2 = 2L;

        String expected = "------- ------- ------- ------- -O----- -O----- ";
        char token = 'O';

        // when
        grille.addToken(indexC1, token);
        grille.addToken(indexC2, token);

        String actual = grille.display();

        // then
        assertEquals(expected, actual);
    }

//    @Test
//    public void ReturnGrid_withOneTokenPassTheLineMaxime() {
//
//        // given
//        Long indexC = 1L;
//
//        String expected = "O------ O------ O------ O------ O------ O------ ";
//
//        // when
//        grille.addToken(indexC);
//        grille.addToken(indexC);
//        grille.addToken(indexC);
//        grille.addToken(indexC);
//        grille.addToken(indexC);
//        grille.addToken(indexC);
//        grille.addToken(indexC);
//
//        String actual = grille.display();
//
//        // then
//        assertEquals(expected, actual);
//    }

    @Test
    public void ReturnGrid_withTwoTokenDifferent() {

        // given
        Long indexC1 = 1L;
        Long indexC2 = 1L;
        char c1 = 'O';
        char c2 = '*';
        String expected = "------- ------- ------- ------- *------ O------ ";
        // when
        grille.addToken(indexC1, c1);
        grille.addToken(indexC2, c2);
        String actual = grille.display();

        // then
        assertEquals(expected, actual);

    }

    @Test
    public void ReturnState_withCellueGiven() {
        // given
        Long indexL = 6L;
        Long indexC = 1L;
        String[] grilleT = { "-------", "-------", "-------", "-------", "-------", "O------" };
        Grille grilleGiven = new Grille(grilleT);
        char expected = 'O';

        // when
        char actual = grilleGiven.getCellue(indexL, indexC);
        // then
        assertEquals(expected, actual);
    }

    @Test
    public void ReturnState_withCellueGivenStar() {
        // given
        Long indexL = 6L;
        Long indexC = 1L;
        String[] grilleT = { "-------", "-------", "-------", "-------", "-------", "*------" };
        Grille grilleGiven = new Grille(grilleT);
        char expected = '*';

        // when
        char actual = grilleGiven.getCellue(indexL, indexC);
        // then
        assertEquals(expected, actual);
    }

    @Test
    public void ReturnState_withCellueGivenColone() {
        // given
        Long indexL = 6L;
        Long indexC = 2L;
        String[] grilleT = { "-------", "-------", "-------", "-------", "-------", "-*-----" };
        Grille grilleGiven = new Grille(grilleT);
        char expected = '*';

        // when
        char actual = grilleGiven.getCellue(indexL, indexC);
        // then
        assertEquals(expected, actual);
    }

    @Test
    public void ReturnState_withCellueGivenColoneAndLine() {
        // given
        Long indexL = 5L;
        Long indexC = 2L;
        String[] grilleT = { "-------", "-------", "-------", "-------", "-O-----", "-*-----" };
        Grille grilleGiven = new Grille(grilleT);
        char expected = 'O';

        // when
        char actual = grilleGiven.getCellue(indexL, indexC);
        // then
        assertEquals(expected, actual);
    }

}
